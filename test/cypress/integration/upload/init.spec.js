import 'cypress-file-upload'

describe('Landing', () => {
  beforeEach(() => {
    cy.visit('/')
  })
  it('.should() - assert that <title> is correct', () => {
    cy.title().should('include', 'MAT2')
  })
})

describe('Upload page tests', () => {
  beforeEach(() => {
    cy.server()
    cy.route({
      method: 'GET',
      url: '/api/*',
      response: ['.png', '.jpg']
    }).as('supportedExtensions')
    cy.visit('/')
    cy.wait('@supportedExtensions')
  })

  it('allows uploading a file', () => {
    cy.route({
      method: 'POST',
      url: '/api/upload',
      response: {
        "output_filename": "test-image.cleaned.png",
        "key": "akey",
        "meta": {
          "SignificantBits": "8 8 8",
          "Software": "Shutter"
        },
        "meta_after": {},
        "download_link": "http://backend.gnu/api/download/akey/test-image.cleaned.png"
      }
    }).as('postFile')

    const fileName = 'test-image.png'

    cy.fixture(fileName).then(fileContent => {
      cy.get('.uppy-Dashboard-dropFilesTitle').upload(
        { fileContent, fileName, mimeType: 'image/png' },
        { subjectType: 'drag-n-drop' }
      )
    })

    cy.get(".uppy-DashboardItem-name").contains("test-image")
    cy.get(".uppy-StatusBar-actions > button").click()

    cy.wait('@postFile').then((xhr) => {
      expect(xhr.requestBody).to.deep.equal({
        file: "iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAMAAAC67D+PAAAAA1BMVEVYWFjSB0xIAAAADElEQVQImWNgoCcAAABuAAEdQFfVAAAAAElFTkSuQmCC",
        file_name: "test-image.png"
      })
      expect(xhr.requestHeaders).to.have.property('Content-Type')
    })
    cy.get(".q-toolbar").contains("Cleaned Files")
    cy.get(".q-item__section").contains("test-image.cleaned.png")
    cy.get('.q-item__section a')
      .should('have.attr', 'href').and('include', 'test-image.cleaned.png')
  })

  it('rejects not supported file extensions', () => {
    const fileName = 'not-supported.json'

    cy.fixture(fileName).then(fileContent => {
      cy.get('.uppy-Dashboard-dropFilesTitle').upload(
        { fileContent, fileName, mimeType: 'application/json' },
        { subjectType: 'drag-n-drop' }
      )
    })
    cy.wait(300)
    cy.get(".uppy-Informer").contains("You can only upload: .png, .jpg")
  })
})
