const { Plugin } = require('@uppy/core')
const cuid = require('cuid')
const Translator = require('@uppy/utils/lib/Translator')
const settle = require('@uppy/utils/lib/settle')
const limitPromises = require('@uppy/utils/lib/limitPromises')
import axios from 'axios'

export default class JSONUploader extends Plugin {
  constructor (uppy, opts) {
    super(uppy, opts)
    this.type = 'uploader'
    this.id = this.opts.id || 'JSONUpload'
    this.title = 'JSONUpload'

    this.defaultLocale = {
      strings: {
        timedOut: 'Upload stalled for %{seconds} seconds, aborting.'
      }
    }

    // Default options
    const defaultOptions = {
      headers: {},
      limit: 0
    }

    // Merge default options with the ones set by user
    this.opts = Object.assign({}, defaultOptions, opts)

    // i18n
    this.translator = new Translator([ this.defaultLocale, this.uppy.locale, this.opts.locale ])
    this.i18n = this.translator.translate.bind(this.translator)

    this.handleUpload = this.handleUpload.bind(this)

    // Simultaneous upload limiting is shared across all uploads with this plugin.
    if (typeof this.opts.limit === 'number' && this.opts.limit !== 0) {
      this.limitUploads = limitPromises(this.opts.limit)
    } else {
      this.limitUploads = (fn) => fn
    }
  }

  getOptions (file) {
    const overrides = this.uppy.getState().xhrUpload
    const opts = {
      ...this.opts,
      ...(overrides || {}),
      ...(file.xhrUpload || {}),
      headers: {}
    }
    Object.assign(opts.headers, this.opts.headers)
    if (overrides) {
      Object.assign(opts.headers, overrides.headers)
    }
    if (file.xhrUpload) {
      Object.assign(opts.headers, file.xhrUpload.headers)
    }

    return opts
  }
  upload (file, current, total) {
    const opts = this.getOptions(file)
    this.uppy.log(`uploading ${current} of ${total}`)
    return new Promise((resolve, reject) => {
      const id = cuid()
      const reader = new FileReader()
      reader.readAsDataURL(file.data)
      const self = this
      const CancelToken = axios.CancelToken
      let cancel
      reader.onloadend = function () {
        const base64result = reader.result.split(',')[1]
        const config = {
          cancelToken: new CancelToken(function executor (c) {
            // An executor function receives a cancel function as a parameter
            cancel = c
          }),
          onUploadProgress: function (progressEvent) {
            self.uppy.log(`[XHRUpload] ${id} progress: ${progressEvent.loaded} / ${progressEvent.total}`)
            self.uppy.emit('upload-progress', file, {
              uploader: this,
              bytesUploaded: progressEvent.loaded,
              bytesTotal: progressEvent.total
            })
          }
        }
        self.uppy.log(`[XHRUpload] ${id} started`)
        axios.post(opts.endpoint, {
          file: base64result,
          file_name: file.meta.name
        }, config).then(function (response) {
          self.uppy.log(`[XHRUpload] ${id} finished`)
          self.uppy.emit('upload-success', file, response)
          return resolve(file)
        })
          .catch(function (error) {
            if (axios.isCancel(error)) {
              console.log('Request canceled', error.message)
            } else {
              self.uppy.log(`[XHRUpload] ${id} errored`)
              reject(new Error(error.message))
              self.uppy.emit('upload-error', file, error)
            }
          })
          .finally(function () {
            self.uppy.log(`[XHRUpload] ${id} done`)
          })
      }
      this.uppy.on('file-removed', (removedFile) => {
        if (removedFile.id === file.id) {
          cancel('Operation canceled by the user.')
          reject(new Error('File removed'))
        }
      })
      this.uppy.on('cancel-all', () => {
        cancel('Operation canceled by the user.')
        reject(new Error('Upload cancelled'))
      })
    })
  }

  uploadFiles (files) {
    const actions = files.map((file, i) => {
      const current = parseInt(i, 10) + 1
      const total = files.length

      if (file.error) {
        return () => Promise.reject(new Error(file.error))
      } else {
        this.uppy.emit('upload-started', file)
        return this.upload.bind(this, file, current, total)
      }
    })

    const promises = actions.map((action) => {
      const limitedAction = this.limitUploads(action)
      return limitedAction()
    })

    return settle(promises)
  }

  handleUpload (fileIDs) {
    if (fileIDs.length === 0) {
      this.uppy.log('[XHRUpload] No files to upload!')
      return Promise.resolve()
    }

    this.uppy.log('[XHRUpload] Uploading...')
    const files = fileIDs.map((fileID) => this.uppy.getFile(fileID))

    return this.uploadFiles(files).then(() => null)
  }

  install () {
    this.uppy.addUploader(this.handleUpload)
  }

  uninstall () {
    this.uppy.removeUploader(this.handleUpload)
  }
}
