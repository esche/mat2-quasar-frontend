# MAT2-web Quasar Frontend

## Up and Running
To start developing use `docker-comse up` and if
this was successful you can access the app on: 
`localhost:8080`.

To get the API working checkout: https://0xacab.org/jvoisin/mat2-web.git
and run `docker-compose up` in that folder as well.
This starts the backend on `localhost:8000`
You can start hacking :)

## Configuration
To set the base url of the backend you have to define
`MAT2_API_URL_DEV` for dev builds and `MAT2_API_URL_PROD`
for production builds. If you use the docker environment
this is customizable in the `docker-compose.yml` file.
If none of these are set it will default to `http://localhost:5000/`.
Make sure to have slash at the end ;)

## E2E Tests
Run the E2E tests: `quasar test --e2e cypress`.
We use the cypress.io framework for testing.
The responses from the backend are mocked so it's not
fully E2E.
